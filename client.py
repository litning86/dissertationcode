import os
import sys
import json
import time
import csv
import requests

def main():
    transaction_times = []

    for i in range(1,101):
        start_time = time.time()

        params = {'sender': 'user1', 'recipient': 'x'+str(i), 'amount': 1}
        r = requests.get(post('http://localhost:8080/transactions/new', params)

        mine_r = requests.get(http://localhost:8080/mine')
        print(json.loads(mine_r.text))

        end_time = time.time()
        elapsed = end_time - start_time
        transaction_times.append(elapsed)


    with open("transactionTimes", 'w', newline='') as newfile:
        wr = csv.writer(newfile, quoting=csv.QUOTE_ALL)       #produces csv out of gathered transaction times
        wr.writerow(transaction_times)

if __name__ == "__main__":
    main()
