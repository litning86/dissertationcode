import os
import sys
import hashlib
import requests
import json
from time import time
from uuid import uuid4
from flask import Flask, jsonify, request
from pymerkle import MerkleTree, validateProof, validationReceipt
from numba import jit, cuda

'''
Based on tutorials provided by Hackermoon and Medium, avaliable at the links below:
https://hackernoon.com/learn-blockchains-by-building-one-117428612f46
https://mycoralhealth.medium.com/code-your-own-proof-of-stake-blockchain-in-go-610cd99aa658
'''


class Blockchain:
    '''
    sets up the initial blockchain system with the genesis block.
    '''
    def __init__(self):
        mt = MerkleTools()
        self.data = []
        self.chain = []
        self.nodes = set()
        self.new_block(prev_hash='1', proof=4)

    @jit(target ="cuda") #when used will instruct that this function runs on the GPU.
    def valid_block(self, chain):
        '''
        checks the validity of the current blockchain, uses a merkle tree to verify
        that the block currently being added matches belongs in the chain.
        '''
        lastBlock = chain[0]
        current_index = 1

        while current_index < len(chain):
            block = chain[current_index]

            lastBlock_hash = self.hash(lastBlock)
            if block['prev_hash'] != lastBlock_hash:
                return False

        for index in mt:
            if not mt.validate_proof(mt.get_proof(index), mt.get_leaf(index), lastBlock['merkle_root']):
                return False

        return True

    def new_block(self, proof, prev_hash):
        '''
        creation of a new block
        '''
        block = {
            'index': len(self.chain) + 1,
            'data': self.data,
            'time_stamp': time(),
            'proof': proof,
            'merkle_root': mt.get_merkle_root(),
            'prev_hash': prev_hash or self.hash(self.chain[-1]),
        }

        temp_chain = self.chain
        mt.add_leaf(block['index'], block['data'], block['time_stamp'], block['prev_hash'], true)
        mt.make_tree()
        temp_chain.append(block)

        if valid_block(temp_chain):
            self.data = []
            self.chain.append(block)

        return block


    @property
    def lastBlock(self):
        return self.chain[-1]

    @staticmethod
    def hash(block):
        block_string = json.dumps(block, sort_keys=True).encode()
        return hashlib.sha256(block_string).hexdigest()

    def proof_of_work(self, lastBlock):
        '''
        Ensures that the hash of the previous block matches with the current block's
        previous hash value
        '''
        last_proof = lastBlock['proof']
        last_hash = self.hash(lastBlock)

        proof = 0
        while self.valid_proof(last_proof, proof, last_hash) is False:
            proof += 1

        return proof

    @staticmethod
    def valid_proof(last_proof, proof, last_hash):
        guess = f'{last_proof}{proof}{last_hash}'.encode()
        guess_hash = hashlib.sha256(guess).hexdigest()
        return guess_hash[:4] == "0000"



app = Flask(__name__)
blockchain = Blockchain()

#mines a new block for the user to push up to the chain.
@app.route('/mine', methods=['GET'])
def mine():

    lastBlock = blockchain.lastBlock
    proof = blockchain.proof_of_work(lastBlock)

    blockchain.new_transaction(
        sender="0",
        recipient=node_identifier,
        amount=1,
    )

    prev_hash = blockchain.hash(lastBlock)
    block = blockchain.new_block(proof, prev_hash)

    response = {
        'message': "New Block",
        'index': block['index'],
        'transactions': block['transactions'],
        'proof': block['proof'],
        'prev_hash': block['prev_hash'],
    }
    return jsonify(response), 200

#simply returns the full blockchain
@app.route('/chain', methods=['GET'])
def full_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain),
    }
    return jsonify(response), 200

#creates a new transaction in preperation to be pushed up to the blockchain
@app.route('/transactions/new', methods=['POST'])
def new_transaction():
    values = request.get_json()

    required = ['sender', 'recipient', 'amount']
    if not all(k in values for k in required):
        return 'Missing values', 400

    index = blockchain.new_transaction(values['sender'], values['recipient'], values['amount'])

    response = {'message': f'Transaction will be added to Block {index}'}
    return jsonify(response), 201

#This function registers and sets the server node once it is up and running
@app.route('/nodes/register', methods=['POST'])
def send_transactions():
    values = request.get_json()

    node = values.get('nodes')

    parsed_url = urlparse(node)
    if parsed_url.netloc:
        self.nodes.add(parsed_url.netloc)
    elif parsed_url.path:
        self.nodes.add(parsed_url.path)
    else:
        raise ValueError('Invalid URL')

    response = {
        'message': 'New node has been added'
    }
    return jsonify(response), 201


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=8080, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='127.0.0.1', port=port)
